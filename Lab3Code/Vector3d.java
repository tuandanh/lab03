/* 
Tuan Danh Huynh
2032677
*/
public class Vector3d {
    private double x;
    private double y;
    private double z;

    // constructor
    public Vector3d(double length, double width, double height){
        this.x = length;
        this.y = width;
        this.z = height;
    }

    //getter
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
// instance method
    public double magnitude(){
        return Math.sqrt((this.x*this.x+this.y*this.y+this.z*this.z));
    }
    public double dotProduct(Vector3d vector){
        return (vector.getX()*this.x)+(vector.getY()*this.y)+(vector.getZ()*this.z); 
    }
    public Vector3d add(Vector3d vector){
        Vector3d vector2 = new Vector3d(this.x + vector.getX(), this.y + vector.getY(), this.z + vector.getZ());
        return vector2;
    }
}
