import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
public class Vector3dTests {
    @Test
    public void testX(){
        Vector3d vector = new Vector3d(2, 4, 6);
        assertEquals(vector.getX(),2);
    }
    @Test
    public void testY(){
        Vector3d vector = new Vector3d(2, 4, 6);
        assertEquals(vector.getY(),4);
    }
    @Test
    public void testZ(){
        Vector3d vector = new Vector3d(2, 4, 6);
        assertEquals(vector.getZ(),6);
    }
    @Test
    public void testMagnitude(){
        Vector3d vector = new Vector3d(2, 4, 6);
        double x = Math.sqrt(2.0*2.0+4.0*4.0+6.0*6.0);
        assertEquals(vector.magnitude(), x);
    }
    @Test
    public void testDotProduct(){
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vector2 = new Vector3d(2, 3, 4);
        assertEquals(vector.dotProduct(vector2), 13);
    }
    @Test
    public void testAdd(){
        Vector3d vector1 = new Vector3d(1, 1, 2);
        Vector3d vector2 = new Vector3d(2, 3, 4);
        Vector3d vector3 = new Vector3d(3, 4, 6);
        Vector3d vector4 = vector1.add(vector2);
        assertEquals(vector3.getX(), vector4.getX());
        assertEquals(vector3.getY(), vector4.getY());
        assertEquals(vector3.getZ(), vector4.getZ());
    }
}
